#ifndef TMATRIX_HH
#define TMATRIX_HH

#include "TSize.hh"
#include "TVector.hh"
#include "CNumber.hh"
#include <iostream>


/*
 *  This class represents matrix, it includes board filled with TVector and methods operating with this class.
 */
template<typename TType, int TSize>
class TMatrix
{
  /*
   * Matrix's body.
   */
  TVector<TType, TSize> _Tab[TSize];

  public:

 /*
  *  Method which gives acces to Matrix's rows.
  */
 TVector<TType, TSize>   operator [](int element)const{return this->_Tab[element];}

  /*
   * Method which allows to write in TVector<TType, TSize> to Matrix's row.
   */
 TVector<TType, TSize>&  operator [](int element)     {return _Tab[element];}

 /*
  *  Method which allows to multiply Matrix's row by another TVector.
  */
 TVector<TType, TSize> operator * (const TVector<TType, TSize> vec)const;
 
  /*
   * Method brings Matrix to Matrix filled with 0 under diagonal then counts Determinant.
   */
  TType Determinant()const;

  /*
   * Method checks if in Matrix are some rows or columns filled with 0.
   */
  int Zero()const;

  /*
   * Method counts Determinant of Matrix
   */
  TType Det();

  /*
   * Method transposes Matrix.
   */
  TMatrix<TType, TSize> Transposition()const;

  /*
   * Method checks if in Matrix are same rows or columns.
   */
  int CheckSameColOrRow()const;

  /*
   * Method which allows to multiply Matrix by another Matrix.
   */
  TMatrix<TType, TSize> operator *(const TMatrix<TType, TSize> &mat)const;

  TMatrix<TType, TSize> operator + (const TMatrix<TType, TSize> &mat)const;

  TMatrix<TType, TSize> MatRepresent2(const CNumber & CN)const;

  TMatrix<TType, TSize> MatRepresent4(const CNumber & CN)const;

  bool operator == (const CNumber &CN)const;
};
  
  template<typename TType, int TSize>
  bool TMatrix<TType, TSize>::operator == (const CNumber &CN)const{
  if(TSize==2){
  if (this->_Tab[0][0]==CN.re && this->_Tab[0][1]==-CN.im && this->_Tab[1][0]==CN.im && this->_Tab[1][1]==CN.re)
  {
    return true;
  }else
  {
    return false;
  }
  
  }
  if(TSize==4){
  if (this->_Tab[0][0]==CN.re && this->_Tab[0][3]==CN.im && this->_Tab[1][1]==CN.re && this->_Tab[1][2]==CN.im && this->_Tab[2][1]==-CN.im && this->_Tab[2][2]==CN.re && this->_Tab[3][0]==-CN.im && this->_Tab[3][3]==CN.re)
  {
    return true;
  }else
  {
    return false;
  }}
  return false;
}


/*
 * Function enters data to object which is Matrix's class using operator's overload.
 * Parameters:
 *   StreamIn - reference to input stream,
 *   matrix - reference to given object Matrix's class.
 * Returns:
 *   Function returns reference to input stream.
 */
template<typename TType, int TSize>
std::istream &operator >> (std::istream &StreamIn, TMatrix<TType, TSize> &matrix){
    TType value;
    for (int i = 0; i < TSize; i++){
        for (int j = 0; j < TSize; j++)
        {
            StreamIn >> value;
            matrix[i][j]=value;
        }
    }
    return StreamIn;
}

/*
 * Function display object which is Matrix's class using operator's overload.
 * Parameters:
 *   StreamOut - reference to output stream,
 *   matrix - reference to given object Matrix's class.
 * Returns:
 *   Function returns reference to output stream.
 */
template<typename TType, int TSize>
std::ostream &operator<<(std::ostream &StreamOut, const TMatrix<TType, TSize> &matrix){
    int j;
    for (int i = 0; i < TSize; i++){
        for (j = 0; j < TSize; j++)
        {
        StreamOut << "\t" << matrix[i][j];
         if (j == TSize - 1)
        {
            std::cout << std::endl;
        }}}
    return StreamOut;
}

/*
 * Method transposes Matrix.
 * Returns:
 *   Function returns transposed Matrix.
 */
template<typename TType, int TSize>
TMatrix<TType, TSize> TMatrix<TType, TSize>::Transposition()const{
    TMatrix<TType, TSize> mat;
    TVector<TType, TSize> vec;
    mat = *this;
    for (int i = 0; i < TSize; i++){
        for (int j = 0; j < TSize; j++){
            vec[j]=this->_Tab[j][i];
        }
        
        mat[i]=vec;
    }
    return mat;
}

/*
 * Method checks if in Matrix are some rows or columns filled with 0.
 * Returns:
 *   Information: 1 - there isn't any row or column filled with 0, 0 - there is a row or column filled with 0.
 */
template<typename TType, int TSize>
int TMatrix<TType, TSize>::Zero()const{
    int counter;
    for (int i = 0; i < TSize; i++){
        counter = 0;
        for (int j = 0; j < TSize; j++)
        {
            if (this->_Tab[i][j]==0)
            {
                ++counter;
            }
            if (counter == TSize)
            {
                return 0;
            }
            
        }
    }

    for (int i = 0; i < TSize; i++){
        counter = 0;
        for (int j = 0; j < TSize; j++)
        {
            if (this->_Tab[j][i]==0)
            {
                ++counter;
            }
            if (counter == TSize)
            {
                return 0;
            }
            
        }
    }
    return 1;
}

/*
 * Method checks if in Matrix are same rows or columns.
 * Returns:
 *   Method returns information: 1 - there are same rows or columns, 0 - there aren't any same rows or columns.
 */
template<typename TType, int TSize>
int TMatrix<TType, TSize>::CheckSameColOrRow()const{
    int colorrow;
    TVector<TType, TSize> vec1, vec2;
    int counter = 0;
    TType multiplier;
    TMatrix<TType, TSize> mat;
    mat=*this;

    for (int i = 0; i < TSize; i++){
        vec1=mat[i];
        multiplier=vec1[i];
        for (int j = i + 1; j < TSize; j++)
        {
            vec2=mat[j];
            if (vec2[i]!=0)
            {
                multiplier=multiplier/vec2[i];
            }else
            {
                multiplier=multiplier/1;
            }
            if (vec1==(vec2*multiplier))
            {
                counter++;
            }  
        }                
    }
    if (counter>=1)
    {
        return colorrow = 1;
    }

    mat=mat.Transposition();

for (int i = 0; i < TSize; i++){
        vec1=mat[i];
        multiplier=vec1[i];
        for (int j = i + 1; j < TSize; j++)
        {
            vec2=mat[j];
            if (vec2[i]!=0)
            {
                multiplier=multiplier/vec2[i];
            }else
            {
                multiplier=multiplier/1;
            }
            if (vec1==vec2*multiplier)
            {
                counter++;
            }  
        }                
    }
    if (counter>=1)
    {
        return colorrow = 1;
    }

    return colorrow = 0;
}

/*
 * Method counts Determinant of Matrix.
 * Returns:
 *   Function returns value of counted Determinant.
 */
template<typename TType, int TSize>
TType TMatrix<TType, TSize>::Det(){
    TType result;
    result = 1;
    for (int i = 0; i < TSize; i++)
    {
        result=result*this->_Tab[i][i];
    }
    return result;
}

/*
 * Method brings Matrix to Matrix filled with 0 under diagonal then counts Determinant.
 * Returns:
 *   Function returns value of counted Determinant.
 */
template<typename TType, int TSize>
TType TMatrix<TType, TSize>::Determinant()const{
    TMatrix<TType, TSize> mat;
    mat=*this;
    int i,j,k;
    TVector<TType, TSize> row;
    int minus = 0;
    TType multiplier, det;
    det = 0;
    int zero;
    int colorrow;

    zero=this->Zero();

    if (zero==0)
    {
        return det;
    }

    colorrow=this->CheckSameColOrRow();

    if (colorrow == 1)
    {
        return det;
    }
    
    
    for(i = 0; i < TSize - 1; i++){
        if (mat[i][i]==0)                            // condition check if on diagonal is 0
        {
            for (int r = i + 1; r < TSize; r++)      // loop change rows not to have 0 on diagonal
            {
                if (mat[r][i] != 0)
                {
                    row=mat[i];
                    mat[i]=mat[r];
                    mat[r]=row;
                    ++minus;
                    r=TSize;
                }
            }
        }
      
        for(j = i + 1; j < TSize; j++){              // loops get values under diagonal to 0
            multiplier = mat[j][i] / mat[i][i]*-1;
                for(k = i; k < TSize; k++){
                    mat[j][k] = mat[j][k] + (multiplier * mat[i][k]);
    }}}

    det = mat.Det();                                 
    
        if (minus%2!=0)                              // check how many rows' change was and change sign before det if it is needed
        {
            det=det*(-1);
        }

    return det;
}
/*
template<typename TType, int TSize>
bool TMatrix<TType, TSize>::operator == (const CNumber &CN)const{
  if (this->_Tab[0][0]==CN.re && this->_Tab[0][1]==-CN.im && this->_Tab[1][0]==CN.im && this->_Tab[1][1]==CN.re)
  {
    return true;
  }
  return false;
}*/

/*
 * Method which allows to multiply Matrix's row by another vector.
 * Returns:
 *   Function returns result of the equation.
 */
template<typename TType, int TSize>
TVector<TType, TSize> TMatrix<TType, TSize>::operator * (const TVector<TType, TSize> vec)const{
    TVector<TType, TSize> result;
    for (int i = 0; i <TSize; i++)
    {
        result[i]=this->_Tab[i]&vec;
    }
    return result;
}

/*
 * Method which allows to multiply Matrix by another Matrix.
 * Parameters:
 *   &mat - reference to the second factor of multiplication.
 * Returns:
 *   Function returns result of the equation.
 */
template<typename TType, int TSize>
TMatrix<TType, TSize> TMatrix<TType, TSize> ::operator *(const TMatrix<TType, TSize> &mat)const{
    TMatrix<TType, TSize> result, transpositionmat;
    transpositionmat=mat.Transposition();

    for (int i = 0; i < TSize; i++){
        for (int j = 0; j < TSize; j++){
            result[i][j]=this->_Tab[i]&transpositionmat[j];
        }   
    }
    
    return result;
}

template<typename TType, int TSize>
TMatrix<TType, TSize> TMatrix<TType, TSize>::operator + (const TMatrix<TType, TSize> &mat)const{
    TMatrix<TType, TSize> result;

    for (int i = 0; i < TSize; i++){
        for (int j = 0; j < TSize; j++){
            result[i][j]=this->_Tab[i][j]+mat._Tab[i][j];
        }
    }
    return result;
}

template<typename TType, int TSize>
TMatrix<TType, TSize> TMatrix<TType, TSize>::MatRepresent2(const CNumber & CN)const{
    TMatrix<TType, TSize> mat;
    mat._Tab[0][0]=CN.re;
    mat._Tab[0][1]=-CN.im;
    mat._Tab[1][0]=CN.im;
    mat._Tab[1][1]=CN.re;
    return mat;
}

template<typename TType, int TSize>
TMatrix<TType, TSize> TMatrix<TType, TSize>::MatRepresent4(const CNumber & CN)const{
    TMatrix<TType, TSize> mat;

    for (int i = 0; i < TSize; i++){
        for (int j = 0; j < TSize; j++){
            mat[i][j]=0;
        }
    }
    

    mat._Tab[0][0]=CN.re;
    mat._Tab[0][3]=CN.im;
    mat._Tab[1][1]=CN.re;
    mat._Tab[1][2]=CN.im;
    mat._Tab[2][1]=-CN.im;
    mat._Tab[2][2]=CN.re;
    mat._Tab[3][0]=-CN.im;
    mat._Tab[3][3]=CN.re;

    return mat;
}


#endif