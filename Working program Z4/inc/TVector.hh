#ifndef TVECTOR_HH
#define TVECTOR_HH

#include "CNumber.hh"
#include <iostream>


/*
 *  This class represents vector, it includes board and methods operating with this class.
 */
template <typename TType, int TSize>
class TVector
{
  /*
   * Vector's body.
   */
  TType _Tab[TSize];

public:

  /*
   * Method gives acces to private Vector's elements.
   */
  TType   operator [](int Ind) const {return _Tab[Ind];}
  
  /*
   * Method allows to write value to Vector.
   */
  TType&  operator [](int Ind) {return _Tab[Ind];}

  /*
   * Method counts inner product of given object Vector's class and first vector.
   */
  TType   operator & (const TVector<TType, TSize> &vec) const;

  /*
   * Method adds given object Vector's class to first vector.
   */
  TVector<TType, TSize>  operator + (const TVector<TType, TSize> &vec2)const;

  /*
   * Method substracts given object Vector's class from first vector.
   */
  TVector<TType, TSize>  operator - (const TVector<TType, TSize> &vec)const;

  /*
   * Method multiplies given double by first vector.
   */
  TVector<TType, TSize>  operator * (const TType &component)const;

  /*
   * Method divides object Vector's class by given double.
   */
  TVector<TType, TSize>  operator / (const TType  &factor)const;

  /*
   * Method compare two Vectors.
   */
  int operator == (const TVector<TType, TSize> &vec)const;
};

/*
 * Function enters data to object which is Vector's class using operator's overload.
 * Parameters:
 *   StreamIn - reference to input stream,
 *   matrix - reference to given object Vector's class.
 * Returns:
 *   Function returns reference to input stream.
 */
template<typename TType, int TSize>
std::istream &operator>>(std::istream &StreamIn, TVector<TType, TSize> &vec){
    for (int i = 0; i < TSize; i++){
        StreamIn >> vec[i];
    }
    return StreamIn;
}

/*
 * Function display object which is Vector's class using operator's overload.
 * Parameters:
 *   StreamIn - reference to input stream,
 *   matrix - reference to given object Vector's class.
 * Returns:
 *   Function returns reference to output stream.
 */
template<typename TType, int TSize>
std::ostream &operator<<(std::ostream &StreamOut, const TVector<TType, TSize> &vec){
  for (int i = 0; i < TSize; i++)
  {
      StreamOut << "\t" << vec[i];
      if (i == (TSize - 1))
      {
          StreamOut << std::endl;
      }
  }
  return StreamOut;
}

/*
 * Method adds given object Vector's class to first vector.
 * Parameters:
 *   vec - reference to object Vector's class (object cannot be changed).
 * Returns:
 *   Function returns result of operation.
 */
template<typename TType, int TSize>
TVector<TType, TSize> TVector<TType, TSize>::operator+ (const TVector<TType, TSize> &vec) const{
    TVector<TType, TSize> result;
    for (int i = 0; i < TSize; i++)
    {
        result[i]=this->_Tab[i]+vec[i];
    }
    return result;
}

/*
 * Method substracts given object Vector's class from first vector.
 * Parameters:
 *   vec - reference to object Vector's class (object cannot be changed).
 * Returns:
 *   Function returns result of operation.
 */
template<typename TType, int TSize>
TVector<TType, TSize> TVector<TType, TSize>::operator - (const TVector<TType, TSize> &vec)const{
    TVector<TType, TSize> result;
    for (int i = 0; i < TSize; i++)
    {
        result[i]=this->_Tab[i]-vec[i];
    }
    return result;
}

/*
 * Method counts inner product of given object Vector's class and first vector.
 * Parameters:
 *   vec - reference to object Vector's class (object cannot be changed).
 * Returns:
 *   Function returns result of operation.
 */
template<typename TType, int TSize>
TType TVector<TType, TSize>::operator & (const TVector<TType, TSize> &vec) const
{
  TType result;
  result = 0;
  int Ind = -1;
  for(TType WspVectora:_Tab) result +=WspVectora*vec[++Ind];
  return result;
}

/*
 * Method multiplies given float by first vector.
 * Parameters:
 *  component - reference to given float (object cannot be changed).
 * Returns:
 *   Function returns result of operation.
 */
template<typename TType, int TSize>
TVector<TType, TSize>  TVector<TType, TSize>::operator * (const TType &component)const{
    TVector<TType, TSize> result;
    for (int i = 0; i < TSize; i++)
    {
        result[i]=this->_Tab[i]*component;
    }
    return result;
}

/*
 * Method divides object Vector's class by given float.
 * Parameters:
 *   factor - reference to double.
 * Returns:
 *   Function returns result of operation.
 */
template<typename TType, int TSize>
TVector<TType, TSize> TVector<TType, TSize>::operator / (const TType &factor)const{
    TVector<TType, TSize> result;
    for (int i = 0; i < TSize; i++)
    {
        result[i]=this->_Tab[i]/factor;
    }
    return result;
}

/*
 * Method compares two Vectors
 * Parameters:
 *   vec - reference to ovject Vector's class.
 * Returns:
 *   Function returns information: 1 - Vectors are the same, 0 - Vectors aren't the same.
 */
template<typename TType, int TSize>
int TVector<TType, TSize>::operator==(const TVector<TType, TSize> &vec)const{
    int counter = 0;
    for (int i = 0; i < TSize; i++)
    {
        if (this->_Tab[i]==vec._Tab[i])
        {
        ++counter;
        }
        
        if (counter == TSize)
        {
            return 1;
        }  
    }
    return 0;
}


#endif