#ifndef CNUMBER_HH
#define CNUMBER_HH
#include<iostream>
#include<cmath>
#include<iomanip>



/*
 * This struct represents complex number, it includes real and imaginary part and methods operating with this struct.
 */
struct  CNumber {

  /*
   * Real part.
   */
  double   re;   

  /*
   * Imaginary part.
   */ 
  double   im;    

  
  /*
   * Operator's overload udes to add Complex Number to another one.
   */
  CNumber  operator + (const CNumber & CN)const;

  /*
   * Operator's overload used to substract Complex Number from another one.
   */
  CNumber  operator - (const CNumber & CN)const;

  /*
   * Operator's overload used to multiply Complex Number by another one.
   */
  CNumber  operator * (const CNumber & CN)const;

  /*
   * Operator's overload used to divide Complex Number by another one.
   */
  CNumber  operator / (const CNumber & CN)const;

  /*
   * Operator's overload used to divide Complex Number by double.
   */
  CNumber operator / (const double & factor)const;

  /*
   * Operator's overload for Complex Numbers, works the same as in built-in types.
   */
  void operator += (const CNumber & CN);

  /*
   * Operator's overload for multiply Complex Number by double.
   */
  CNumber operator * (const double & factor)const;

  /*
   * Operator's overload for Complex Numbers, works the same as in builtin types.
   */
  bool operator == (const CNumber & CN)const;

  /*
   * Returns interface.
   */
  CNumber Interface()const;

  /*
   * Count Complex Number's squared module.
   */
  double Module2()const;

  /*
   * Operator's overload used to read in double's value to Complex Number's value.
   */
  CNumber &operator =  (double number);

  /*
   * Operator's overload used to check if Complex Number and double are the same.
   */
  bool operator == (double number);

  /*
   * Operator's overload used to check if Complex Number and double are different.
   */
  bool operator != (double number);

 /* bool operator == (const TMatrix<double, 2> &mat)const;*/

  /*bool operator == (const TMatrix<double, 4> &mat)const;*/
};

/*
 * Function displays Complex Number using oerator's overload.
 */
std::ostream & operator << (std::ostream & StreamOut, const CNumber CN);

/*
 * Function read in Complex Number.
 */
std::istream & operator >> (std::istream &StreamIn, CNumber & CN);

void CNMatrixTest();

#endif