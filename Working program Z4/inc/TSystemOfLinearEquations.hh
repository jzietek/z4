#ifndef TSYSTEMOFLINEAREQUATIONS_HH
#define TSYSTEMOFLINEAREQUATIONS_HH

#include <iostream>
#include <cmath>
#include "TVector.hh"
#include "TMatrix.hh"
#include "CNumber.hh"

/*
 *  This class represents system of linear equations,
 *  it includes origin matrix, free-words vector, results vecctor, mistake vector,results and methods operating with this class.
 */
template<typename TType, int TSize>
class TSystemOfLinearEquations
{
  /*
   * Origin transposed TMatrix.
   */
  TMatrix<TType, TSize> TransposedOriginMat;

  /*
   * Free words TVector.
   */
  TVector<TType, TSize> FreeWordsVector;

  /*
   * Results TVector.
   */
  TVector<TType, TSize> ResultsVector;

  /*
   * Mistake TVector.
   */
  TVector<TType, TSize> MistakeVector;

  /*
   * Information about results.
   */
  int Results;

public:
  /*
   * Method returns Origin Matrix.
   */
 TMatrix<TType, TSize>   RetMat()const{return TransposedOriginMat;}

  /*
   * Method allows to write Origin TMatrix.
   */
 TMatrix<TType, TSize>&  WriteTransposedOriginMat()     {return TransposedOriginMat;}

 /*
  * Method returns Free Words TVector>.
  */
 TVector<TType, TSize>   RetFreeWordsVector()const{return FreeWordsVector;}

  /*
   * Method allows to write Free Words TVector.
   */
 TVector<TType, TSize>&  WriteFreeWordsVector()     {return FreeWordsVector;}

 /*
  * Method returns Results TVector.
  */
 TVector<TType, TSize>   RetResultsVector()const{return ResultsVector;}

  /*
   * Method allows to write Results TVector.
   */
 TVector<TType, TSize>&  WriteResultsVector()     {return ResultsVector;}

  /*
   * Method returns Mistake TVector.
   */
 TVector<TType, TSize>   RetMistakeVector()const{return MistakeVector;}

  /*
   * Method allows to write Mistake TVector.
   */
 TVector<TType, TSize>&  WriteMistakeVector()     {return MistakeVector;}

  /*
   * Method uses Cramer's Method to count Results TVector.
   */
 TVector<TType, TSize> Cramer();

  /*
   * Metohod returns information about results.
   */
 int RetResults()const{return Results;}

  /*
   * Method allows to write information about results.
   */
 int& WriteResults()  {return Results;}

  /*
   * Method counts Mistake TVector.
   */
 TVector<TType, TSize> CountMistakeVector() const{TVector<TType, TSize> result; return result = ((this->RetMat().Transposition()*ResultsVector)-FreeWordsVector);}
};


/*
 * Function enters data to object which is SystemOfLinearEquations' class using operator's overload.
 * Parameters:
 *   StreamIn - reference to input stream,
 *   System - reference to given object SystemOfLinearEquations' class.
 * Returns:
 *   Function returns reference to input stream.
 */
template<typename TType, int TSize>
std::istream &operator>>(std::istream &StreamIn, TSystemOfLinearEquations<TType, TSize> &System){
    TMatrix<TType, TSize> mat;
    TVector<TType, TSize> vec;
    StreamIn >> mat;
    StreamIn >> vec;
    System.WriteTransposedOriginMat()=mat;
    System.WriteFreeWordsVector()=vec;
    return StreamIn;
}


/*
 * Function display object which is SystemOfLinearEquations' class using operator's overload.
 * Parameters:
 *   StreamOut - reference to output stream,
 *   System - reference to given object SystemOfLinearEquations' class.
 * Returns:
 *   Function returns reference to output stream.
 */
template<typename TType, int TSize>
std::ostream &operator<<(std::ostream &StreamOut, const TSystemOfLinearEquations<TType, TSize> &System){
    StreamOut << std::fixed << std::setprecision(2);
    StreamOut << "Matrix A^T:"                                                 << std::endl << std::endl;
    StreamOut << System.RetMat()                                               << std::endl << std::endl;
    StreamOut << "Free words vector b:"                                        << std::endl << std::endl;
    StreamOut << System.RetFreeWordsVector()                                   << std::endl << std::endl;
    if (System.RetResults()==1){
        StreamOut << "Result x = (";
        for (int i = 1; i <= TSize; i++)
        {
            std::cout << "x_" << i;
            if (i!=TSize)
            {
                StreamOut << ", ";
            }};  
        StreamOut << ")"<< std::endl                                                                                                            << std::endl;
        StreamOut << System.RetResultsVector()                                                                                     << std::endl << std::endl;
        StreamOut << "Mistake vector:\tAx-b :" << std::endl << std::endl << std::scientific << std::setprecision(1) << System.RetMistakeVector()<< std::endl;
    }
    if (System.RetResults()==0)
    {
        StreamOut << "There are no results." << std::endl;
    }
    if (System.RetResults()==2)
    {
        StreamOut << "There are no results or there is infinity of results." << std::endl;
    }

    return StreamOut;
}

/*
 * Method uses Cramer's Method to count Results Vector.
 * Returns:
 *   Function returns Results Vector.
 */
template<typename TType, int TSize>
TVector<TType, TSize> TSystemOfLinearEquations<TType, TSize>::Cramer(){
    TVector<TType, TSize> result;
    TMatrix<TType, TSize> mat;
    TType det;
    int counter = 0;

    for (int i = 0; i < TSize; i++)
    {
        result[i]=0;
    }
    

    mat=this->RetMat();
    det=mat.Determinant();                          // counts origin matrix's det

    if (det==0)
    {
        ++counter;
    }
    
        for (int j = 0; j < TSize; j++){            // this loop change matrix row with free words vector
            mat=this->RetMat();                     
            mat[j]=this->RetFreeWordsVector();      
            result[j]=mat.Determinant();            // use function Determinant to count det
            if (result[j]==0)
            {
                counter++;                          // counter check how many dets was equal to 0
            }else
            {
                result[j]=result[j]/det;            // divides matrix with free words vector's det by origin matrix's det
            }
        }

        if (det==0)                                 // condition connected with amount of dets equal to 0
            {
                if (counter == TSize + 1)
                {
                    this->WriteResults() = 2;
                    return result;
                }else
                {
                    this->WriteResults() = 0;
                    return result;
                }   
            }

    this->WriteResults() = 1;
    return result;
}

#endif