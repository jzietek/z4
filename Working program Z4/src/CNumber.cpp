#include "CNumber.hh"
#include "TMatrix.hh"


/*
 * Function displays Complex Number using oerator's overload.
 * Parameters:
 *    StreamOut - output stream,
 *    CN - displayed Complex Number
 * Returns:
 *    Function returns reference to output stream.
 */
std::ostream & operator << (std::ostream & StreamOut, const CNumber CN){
  StreamOut << std::fixed << std::setprecision(2) << "(" << CN.re << std::showpos << CN.im << std::noshowpos << "i" << ")";
  return StreamOut;
}

/*
 * Write data into structure.
 * Parameters:
 *    StreamIn - input stream,
 *    CN - structure.
 * Returns:
 *    Function returns reference to input stream.
 */

std::istream & operator >> (std::istream &StreamIn, CNumber & CN){
  char help;
   StreamIn >> help;
   if (StreamIn.fail() || help != '(')
   {
     std::cerr << "Mistake: reading in has not succeded." << std::endl;
     return StreamIn;
   }
   StreamIn >> CN.re;
   if (StreamIn.fail())
   {
     std::cerr << "Mistake: reading in has not succeded." << std::endl;
     return StreamIn;
   }
   StreamIn >> CN.im;
   if (StreamIn.fail())
   {
     std::cerr << "Mistake: reading in has not succeded." << std::endl;
     return StreamIn;
   }
   StreamIn >> help;
   if (StreamIn.fail() || help != 'i')
   {
     std::cerr << "Mistake: reading in has not succeded." << std::endl;
     return StreamIn;
   }
   StreamIn >> help;
   if (StreamIn.fail() || help != ')')
   {
     std::cerr << "Mistake: reading in has not succeded." << std::endl;
     return StreamIn;
   }
   
  return StreamIn;
}



/*
 * Function adds Complex Numbers.
 * Parameters:
 *    CN - added Complex Number.
 * Returns:
 *    Sum of two Complex Numbers.
 */
CNumber CNumber::operator+(const CNumber & CN)const{
    CNumber result;
    result.im=CN.im+this->im;
    result.re=CN.re+this->re;
    return result;
}

/*
 * Function substracts Complex Numbers.
 * Parameters:
 *    CN - equation's factor.
 * Returns:
 *    Returns result of the equation.
 */
CNumber CNumber::operator-(const CNumber & CN)const{
    CNumber result;
    result.im=this->im-CN.im;
    result.re=this->re-CN.re;
    return result;
}

/*
 * Multiply two Complex Numbers.
 * Parameters:
 *    CN - equation's factor.
 * Returns:
 *    Returns result of the equations.
 */
CNumber CNumber::operator*(const CNumber & CN)const{
    CNumber result;
    result.im=(this->re*CN.im)+(this->im*CN.re);
    result.re=(this->re*CN.re)-(this->im*CN.im);
    return result;
}

/*
 * Operator's overload for Complex Numbers, works the same as in built-in types.
 * Parameters:
 *    CN - added Complex Number.
 */
void CNumber::operator += (const CNumber & CN){
  this->re=this->re+CN.re;
  this->im=this->im+CN.im;
}

/*
 * Returns interface.
 * Returns:
 *    Returns interface.
 */
CNumber CNumber::Interface()const{
    CNumber result;
    result.re=this->re;
    result.im=-(this->im);
    return result;
}

/*
 * Count Complex Number's module^2.
 * Returns:
 *    Returns Complex Number's module^2.
 */
double CNumber::Module2()const{
    double result;
    result=this->im*this->im+this->re*this->re;
    return result;
}

/*
 * Function multiply Complex Number by double.
 * Parameters:
 *    factor - multiplier.
 * Returns:
 *    Result of the equation.
 */
CNumber CNumber::operator * (const double & factor)const{
  CNumber CN = *this;
    if(factor!=0){
    CN.re=(CN.re*factor);
    CN.im=(CN.im*factor);}
    return CN;
}

/*
 * Function divide Complex Number by double.
 * Parameters:
 *    factor - divider.
 * Returns:
 *    Result of the equation.
 */
CNumber CNumber::operator / (const double & factor)const{
  CNumber CN = *this;
    if(factor!=0){
    CN.re=(CN.re/factor);
    CN.im=(CN.im/factor);
    }
    return CN;
}

/*
 * Divide Complex Number by another one.
 * Parameters:
 *    CN - factor of the equation.
 * Returns:
 *    Returns result of the equation.
 */
CNumber CNumber::operator / (const CNumber & CN)const{
    CNumber result;
    result=*this*CN.Interface();
    result=result/CN.Module2();
    return result;
}

/*
 * Function compares two Complex Numbers.
 * Parameters:
 *    CN - factor.
 * Returns:
 *    Information.
 */
bool CNumber::operator==(const CNumber & CN)const{
  if (this->re==CN.re && this->im == CN.im)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/*
 * Operator's overload used to read in double's value to Complex Number's value..
 * Parameters:
 *    number - written in number.
 * Returns:
 *    Pointer to Complex Numbwr.
 */
CNumber &CNumber::operator = (double number){
  this->re = number;
  this->im = 0;
  return *this;
}


/*
 * Operator's overload used to check if Complex Number and double are the same.
 * Parameters:
 *    number - compared number.
 * Returns:
 *    Returns true or false.
 */
bool CNumber::operator == (double number){
  if (this->re==number && this->im==0)
  {
    return true;
  }else

  return false;
}

/*
 * Operator's overload used to check if Complex Number and double are different.
 * Parameters:
 *    number - compared number.
 * Returns:
 *    Returns true or false.
 */
bool CNumber::operator != (double number){
  if (this->re!=number || this->im!=0)
  {
    return true;
  }
  
  return false;
}

/*
bool CNumber::operator == (const TMatrix<double, 2> &mat)const{
  if (mat[0][0]==this->re && mat[0][1]==-this->im && mat[1][0]==this->im && mat[1][1]==this->re)
  {
    return true;
  }
  return false;
}*/

/*bool CNumber::operator == (const TMatrix<double, 4> &mat)const{
  if (mat[0][0]==this->re && mat[0][3]==this->im && mat[1][1]==this->re && mat[1][2]==this->im && mat[2][1]==-this->im && mat[2][2]==this->re && mat[3][0]==-this->im && mat[3][3]==this->re)
  {
    return true;
  }
  return false;
}*/

void CNMatrixTest(){
  TMatrix<double, 2> mat1, mat2, mat3, mat4;
       CNumber CN1, CN2, CN3, CN4;
       CN1.re = 1;
       CN1.im = 5;
       CN2.re = 6;
       CN2.im = -2;

       mat1=mat1.MatRepresent2(CN1);
       mat2=mat2.MatRepresent2(CN2);

       std::cout << "***********************************************" << std::endl << std::endl;
       std::cout << "Complex number's 2x2 matrix representation test" << std::endl << std::endl;
       std::cout << "Complex Number: 1+5i in Matrix representation: " << std::endl << std::endl << std::fixed << mat1 << std::endl;

       mat3=mat1+mat2;

       std::cout << "Result of the (1+5i)+(6-2i) in Matrix representation: " << std::endl << std::endl;
       std::cout << mat3 << std::endl;

       CN3=CN1+CN2;
 
       std::cout << "Result of the (1+5i)+(6-2i): " << std::endl << std::endl;
       std::cout << "\t" << CN3 << std::endl << std::endl;

       std::cout << "Comparing two types of Complex Number Representation: " << std::endl << std::endl;
       if (mat3==CN3)
       {
         std::cout << "They are the same." << std::endl << std::endl;
       }else
       {
         std::cout << "They are not the same." << std::endl << std::endl;
       }

       CN4=CN1*CN2;
       
       std::cout << "Result of the (1+5i)*(6-2i): " << std::endl << std::endl;
       std::cout << "\t" << CN4 << std::endl << std::endl;

       mat4=mat1*mat2;

       std::cout << "Result of the (1+5i)*(6-2i) in Matrix representation: " << std::endl << std::endl;
       std::cout << mat4 << std::endl << std::endl;

       std::cout << "Comparing two types of Complex Number Representation: " << std::endl << std::endl;
       if (mat4==CN4)
       {
         std::cout << "They are the same." << std::endl << std::endl;
       }else
       {
         std::cout << "They are not the same." << std::endl << std::endl;
       }

       std::cout << "Module^2 of the 1+5i: " << std::endl << std::endl;
       std::cout << CN1.Module2() << std::endl << std::endl;

       std::cout << "Module of the 1+5i using Matrix Representation: " << std::endl << std::endl;
       std::cout << mat1.Determinant() << std::endl << std::endl;

       TMatrix<double, 4> Mat1, Mat2, Mat3, Mat4;
       CNumber cn1, cn2, cn3, cn4;
       cn1.re = 1;
       cn1.im = 5;
       cn2.re = 6;
       cn2.im = -2;

       Mat1=Mat1.MatRepresent4(cn1);
       Mat2=Mat2.MatRepresent4(cn2);

       std::cout << "***********************************************" << std::endl << std::endl;
       std::cout << "Complex number's 4x4 matrix representation test" << std::endl << std::endl;
       std::cout << "Complex Number: 1+5i in Matrix representation: " << std::endl << std::endl << std::fixed << Mat1 << std::endl;

       Mat3=Mat1+Mat2;

       std::cout << "Result of the (1+5i)+(6-2i) in Matrix representation: " << std::endl << std::endl;
       std::cout << Mat3 << std::endl;

       cn3=cn1+cn2;
 
       std::cout << "Result of the (1+5i)+(6-2i): " << std::endl << std::endl;
       std::cout << "\t" << cn3 << std::endl << std::endl;

       std::cout << "Comparing two types of Complex Number Representation: " << std::endl << std::endl;
       if (Mat3==cn3)
       {
         std::cout << "They are the same." << std::endl << std::endl;
       }else
       {
         std::cout << "They are not the same." << std::endl << std::endl;
       }

       cn4=cn1*cn2;
       
       std::cout << "Result of the (1+5i)*(6-2i): " << std::endl << std::endl;
       std::cout << "\t" << cn4 << std::endl << std::endl;

       Mat4=Mat1*Mat2;

       std::cout << "Result of the (1+5i)*(6-2i) in Matrix representation: " << std::endl << std::endl;
       std::cout << Mat4 << std::endl << std::endl;

       std::cout << "Comparing two types of Complex Number Representation: " << std::endl << std::endl;
       if (Mat4==cn4)
       {
         std::cout << "They are the same." << std::endl << std::endl;
       }else
       {
         std::cout << "They are not the same." << std::endl << std::endl;
       }

       std::cout << "Module^4 of the 1+5i: " << std::endl << std::endl;
       std::cout << pow(cn1.Module2(), 2) << std::endl << std::endl;

       std::cout << "Module of the 1+5i using Matrix Representation: " << std::endl << std::endl;
       std::cout << Mat1.Determinant() << std::endl << std::endl;
}