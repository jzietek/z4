#include <iostream>
#include "TVector.hh"
#include "TSize.hh"
#include "CNumber.hh"
#include "TMatrix.hh"
#include "TSystemOfLinearEquations.hh"



int main()
{
       char letter;
       std::cin >> letter;
       switch (letter){
       case 'r':
              std::cout << std::endl << "System of linear equations with real numbers." << std::endl << std::endl;
              TSystemOfLinearEquations<double, SIZE> system;
              std::cin >> system;
              system.WriteResultsVector()=system.Cramer();
              system.WriteMistakeVector()=system.CountMistakeVector();
              std::cout << system << std::endl;
              break;
       
       case 'z':
              std::cout << std::endl << "System of linear equations with complex numbers." << std::endl << std::endl;
              TSystemOfLinearEquations<CNumber, SIZE> System;
              std::cin >> System;
              System.WriteResultsVector()=System.Cramer();
              System.WriteMistakeVector()=System.CountMistakeVector();
              std::cout << System << std::endl;
              break;
       default:
             std::cerr << "Mistake: wrong option. Choose 'r'-for real numbers or 'z'-for complex numbers." << std::endl;
              break;
       }

       CNMatrixTest();

       }